const mockService = require("osprey-mock-service");
const express = require("express");
const parser = require("raml-1-parser");
const path = require("path");
const osprey = require("osprey");

const app = express();

/**
 * The server spins up a mock service that accepts RAML definition and returns a router.
 */

parser
  .loadRAML(path.join(__dirname, "space-api.raml"), { rejectOnErrors: true })
  .then(function(ramlApi) {
    const raml = ramlApi.expand(true).toJSON({ serializeMetadata: false });
    app.use(osprey.server(raml)); // raml file validation
    app.use(mockService(raml));
    const PORT = process.env.PORT || 5000;
    app.listen(PORT, () => {
      console.log(`Server listening on ${PORT}`);
    });
  });
