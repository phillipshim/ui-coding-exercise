# Atlassian UI Assignment
Render UI per required specifications

## User stories
1. Users can navigate spaces by clicking on a
vertical tab on the left.
2. Users can navigate between the entries
and assets tab for a given space.
3. Users can sort (on the client side) by
clicking column headers. (For the
purposes of this test, assume the API does
not support server-side sort)
4. URL Route should contain a Space GUID.
5. Visiting the base URL should redirect to
the first space returned by the /space endpoint

### In retrospect and thoughts
* I really wish I had more time to refine the app, but I am in the middle of other interviews and assignments so couldn't make more time.
*  Proxy errors out sometimes, you might have to reload the app to reach out to correct routes for API requests
* Description box won't get rendered since no particular spaceId has description property and value.
* Unit test fails on a react hook. If I had more time, I would've done component level testing to make sure it renders what it's supposed to render and also integration testing to test user interactions (For ex. hitting on different tabs and spaces)
* Make my react custom more reusable and use it in other components instead of repeating similar network requests in other components, which will automatically handle network failures since the custom hook handles it.

## Running a local copy of the app
- Clone the repo

  ```bash
  git clone git@bitbucket.org:phillipshim/ui-coding-exercise.git
  cd ui-coding-exercise
  ```

- Install the dependencies of the server

  ```bash
  yarn
  ```

  `or`

  ```bash
  npm install
  ```

- Install the dependencies of the client

  ```bash
  cd client
  ```

  ```bash
  yarn
  ```

  ```bash
  npm install
  ```

- Start both backend and frontend servers

 ```bash
  cd ..
 ```

  ```bash
  yarn dev
  ```

  `or`

  ```bash
  npm run dev
  ```

  - Go head and visit the app at localhost:3000 

  - To start the tests on client

  ```bash
  cd client
  ```
  
  ```bash
  yarn test
  ```

  or

  ```bash
  npm run test
  ```