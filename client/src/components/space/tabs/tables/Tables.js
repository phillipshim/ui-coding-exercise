import React from "react";
import { Table } from "antd";

const Tables = props => {
  /**
   *  List of displayed props from both fields and sys objects
    // fields.title
    // fields.summary
    // sys.createdBy
    // sys.updatedBy
    // sys.updatedAt

    Other props not displayed
      // body;
      // id;
      // type;
      // version;
      // space;
      // createdAt;
   */

  if (props.entries) {
    const columns = [
      {
        title: "Title",
        dataIndex: "title",
        sorter: (a, b) => a.title.length - b.title.length
      },
      {
        title: "Summary",
        dataIndex: "summary",
        sorter: (a, b) => a.summary.length - b.summary.length
      },
      {
        title: "Created By",
        dataIndex: "createdBy",
        sorter: (a, b) => a.createdBy.length - b.createdBy.length
      },
      {
        title: "Updated By",
        dataIndex: "updatedBy",
        sorter: (a, b) => a.updatedBy.length - b.updatedBy.length
      },
      {
        title: "Last Updated",
        dataIndex: "updatedAt",
        sorter: (a, b) => a.updatedAt.length - b.updatedAt.length
      }
    ];

    const data = props.entries.map((entry, index) => {
      const obj = {
        key: index,
        ...entry.fields,
        ...entry.sys
      };
      return obj;
    });

    return <Table columns={columns} dataSource={data} />;
  }

  if (props.assets) {
    const columns = [
      {
        title: "Title",
        dataIndex: "title",
        sorter: (a, b) => a.title.length - b.title.length
      },
      {
        title: "Content Type",
        dataIndex: "contentType",
        sorter: (a, b) => a.contentType.length - b.contentType.length
      },
      {
        title: "File Name",
        dataIndex: "fileName",
        sorter: (a, b) => a.fileName.length - b.fileName.length
      },
      {
        title: "Created By",
        dataIndex: "createdBy",
        sorter: (a, b) => a.createdBy.length - b.createdBy.length
      },
      {
        title: "Updated By",
        dataIndex: "updatedBy",
        sorter: (a, b) => a.updatedBy.length - b.updatedBy.length
      },
      {
        title: "Last Updated",
        dataIndex: "updatedAt",
        sorter: (a, b) => a.updatedAt.length - b.updatedAt.length
      }
    ];

    const data = props.assets.map((entry, index) => {
      const obj = {
        key: index,
        ...entry.fields,
        ...entry.sys
      };
      return obj;
    });

    return <Table columns={columns} dataSource={data} />;
  }

  return <div>Hello</div>;
  // return <Table columns={columns} dataSource={data} />;
};

export default Tables;
