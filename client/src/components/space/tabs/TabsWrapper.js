import React, { useState, useEffect } from "react";
import Tables from "./tables/Tables";
import axios from "axios";
import { Tabs } from "antd";

const { TabPane } = Tabs;

const TabsWrapper = ({ spaceId }) => {
  // I could use useFetch hook I created here.
  const [entries, setEntries] = useState([]);
  const [assets, setAssets] = useState([]);

  useEffect(() => {
    axios.get(`/space/${spaceId}/entries`).then(response => {
      const { data } = response;
      setEntries(data.items);
    });

    axios.get(`/space/${spaceId}/assets`).then(response => {
      const { data } = response;
      setAssets(data.items);
    });
  }, []);

  // I could lazyload assets component

  return (
    <Tabs defaultActiveKey="1">
      <TabPane tab="Entries" key="1">
        <Tables entries={entries} />
      </TabPane>
      <TabPane tab="Assets" key="2">
        <Tables assets={assets} />
      </TabPane>
      <Tables />
    </Tabs>
  );
};

export default TabsWrapper;
