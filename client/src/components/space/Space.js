import React, { useState, useEffect } from "react";
import axios from "axios";
import Header from "./header/Header";
import TabsWrapper from "./tabs/TabsWrapper";

const Space = props => {
  const initialFields = {
    title: "",
    description: ""
  };

  const { spaceId } = props.match.params;
  const [fields, setFields] = useState(initialFields);
  const [createdBy, setCreatedBy] = useState("4FLrUHftHW3v2BLi9fzfjU"); // default userExample

  useEffect(() => {
    // This fetch will always retreive the same space for id yadj1kx9rmg0 no matter different spaceId
    axios.get(`/space/${spaceId}`).then(response => {
      const { fields, sys } = response.data;
      const createdBy = sys.createdBy;
      console.log("spaceId Sys", sys);
      setFields(fields)
      setCreatedBy(createdBy);
    });
  }, [spaceId]);

  // Render data only associated with the first spaceId
  if (spaceId === "yadj1kx9rmg0") {
    return (
      <div>
        <Header fields={fields} createdBy={createdBy}/>
        <TabsWrapper spaceId={spaceId} />
      </div>
    );
  }

  // Render Not avaliable if it's not the first spaceId
  return <h2>Not Avaliable</h2>;
};

export default Space;
