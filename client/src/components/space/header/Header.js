import React, { useState, useEffect } from "react";
import axios from "axios"
import Heading from "./heading/Heading";
import Description from "./description/Description";

const Header = ({ fields: { title, description }, createdBy }) => {
  const [name, setName] = useState("")

  useEffect(() => {
    axios.get(`/users/${createdBy}`).then(response => {
      const { name } = response.data.fields; 
      console.warn("name", name)
      setName(name);
    })
  }, [])
  
  return (
    <div style={{marginLeft: "30px", marginTop: "30px"}}>
      <Heading title={title} name={name}/>
      <Description description={description}/>
    </div>
  );
};

export default Header;
