import React from "react";

const Heading = ({ title, name }) => {
  return (
    <h1>
      {title}{" "}
      <span style={{ fontStyle: "italic", fontSize: "0.7rem" }}>created by {name}</span>
    </h1>
  );
};

export default Heading;
