import React from "react";

const Description = ({ description }) => {
  // description never gets populated because no inidividual space has description property.
  return <div>{description}</div>;
};

export default Description;
