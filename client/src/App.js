import React, { useEffect } from "react";
import { Route, Link } from "react-router-dom";
import { Layout, Menu } from "antd";

import Space from "./components/space/Space";
import { useFetchSpace } from "./hooks/hooks";

const { Item } = Menu;
const { Sider } = Layout;

const App = props => {
  const { loading, spaces } = useFetchSpace("/space", []);

  console.log(spaces)

  useEffect(() => {
    const redirect = props => {
      const { pathname } = props.location;
      // Redirect to the first space if user hits root or spaceexplorer
      if ((pathname === "/" || pathname === "/spaceexplorer") && spaces.length) {
        props.history.push(`/spaceexplorer/${spaces[0].sys.id}`);
      }
    };
    redirect(props);
  }, [spaces]);

  if (loading) {
    return <div>App is loading...</div>;
  }

  return (
    <Layout>
      <Sider width={200}>
        <Menu
          mode="inline"
          defaultSelectedKeys={["0"]}
          style={{ height: "100%" }}
        >
          {spaces.map((space, index) => (
            <Item key={index}>
              <Link to={`/spaceexplorer/${space.sys.id}`}>
                {space.fields.title}
              </Link>
            </Item>
          ))}
        </Menu>
      </Sider>

      <Route path="/spaceexplorer/:spaceId" component={Space} />
    </Layout>
  );
};

export default App;
