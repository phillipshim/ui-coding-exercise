import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";

const Root = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={App} />
      <Route path="/spaceexplorer" component={App} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(<Root />, document.getElementById("root"));