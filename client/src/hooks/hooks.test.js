import { renderHook, act } from "@testing-library/react-hooks";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";

import { useFetch } from "./hooks";

test("useFetch performs GET request", async () => {
  const initialValue = [];
  const mock = new MockAdapter(axios);

  const mockData = [{}, {}, {}];
  const url = "/space";
  mock.onGet(url).reply(200, mockData);

  const { result, waitForNextUpdate } = renderHook(() =>
    useFetch(url, initialValue)
  );

  expect(result.current.spaces).toEqual([]);
  expect(result.current.loading).toBeTruthy();

  await waitForNextUpdate();

  console.log(result.current)
  console.log(result.current.spaces)
  console.log(result.current.spaces.length)
  expect(result.current.spaces.length).toBe(3);
  expect(result.current.loading).toBeFalsy();
});
