import { useState, useEffect } from "react";
import axios from "axios";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);

export const useFetchSpace = (url, initialValue) => {
  const [spaces, setSpaces] = useState(initialValue);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchSpace = async () => {
      try {
        setLoading(true);
        const response = await axios.get(url);
        if (response.status === 200) {
          const { items } = response.data;
          setSpaces(items);
        }
      } catch (error) {
        MySwal.fire({
          type: "error",
          title: "Loading data error!",
          text: "Error in loading Spaces"
        });
      } finally {
        setLoading(false);
      }
    };
    fetchSpace();
  }, [url]);

  return { loading, spaces };
};
